package com.rilixtech.widget.materialprogressview;

import android.content.Context;

/**
 * Created by moos on 2018/3/16.
 * utils to help develop
 */

class Utils {
  /**
   * change dp to px
   */
  static int dp2px(Context context, float dpValue) {
    float scale = context.getResources().getDisplayMetrics().density;
    return (int) (dpValue * scale + 0.5f);
  }

  /**
   * change sp to px
   */
  static int sp2px(Context context, float spValue) {
    final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
    return (int) (spValue * fontScale + 0.5f);
  }
}
